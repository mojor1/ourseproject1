﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseProject
{
    static class Program
    {
        static void sysout(double[,] a, double[] y, int n)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write($@"{a[i, j]}*x{j + 1}");
                    if (j < n - 1)
                        Console.Write(" + ");
                }
                Console.WriteLine($@" = {y[i]}");
            }
            return;
        }
        static double[] gauss(double[,] a, double[] y, int n)
        {
            double[] x;
            double max;
            int k, index;
            const double eps = 0.00001;  // точность
            x = new double[n];
            k = 0;
            while (k < n)
            {
                // Поиск строки с максимальным a[i][k]
                max = Math.Abs(a[k, k]);
                index = k;
                for (int i = k + 1; i < n; i++)
                {
                    if (Math.Abs(a[i, k]) > max)
                    {
                        max = Math.Abs(a[i, k]);
                        index = i;
                    }
                }
                // Перестановка строк
                if (max < eps)
                {
                    // нет ненулевых диагональных элементов
                    Console.WriteLine("Решение получить невозможно из-за нулевого столбца ");
                    Console.WriteLine($@"{index} матрицы A");
                    return null;
                }
                for (int j = 0; j < n; j++)
                {
                    double temp1 = a[k, j];
                    a[k, j] = a[index, j];
                    a[index, j] = temp1;
                }
                double temp = y[k];
                y[k] = y[index];
                y[index] = temp;
                // Нормализация уравнений
                for (int i = k; i < n; i++)
                {
                    double temp2 = a[i, k];
                    if (Math.Abs(temp2) < eps) continue; // для нулевого коэффициента пропустить
                    for (int j = 0; j < n; j++)
                        a[i, j] = a[i, j] / temp2;
                    y[i] = y[i] / temp2;
                    if (i == k) continue; // уравнение не вычитать само из себя
                    for (int j = 0; j < n; j++)
                        a[i, j] = a[i, j] - a[k, j];
                    y[i] = y[i] - y[k];
                }
                k++;
            }
            // обратная подстановка
            for (k = n - 1; k >= 0; k--)
            {
                x[k] = Math.Round(y[k], 4);
                for (int i = 0; i < k; i++)
                    y[i] = y[i] - a[i, k] * x[k];
            }
            return x;
        }
        static void Main(string[] args)
        {

            int n =0;
            double[,] a = null;
            double[] y = null;
            bool flag = false;
            do
            {
                Console.WriteLine("1. Ввести данные из файла");
                Console.WriteLine("2. Ввести данные с косоли");
                string st = Console.ReadLine();
                switch (st)
                {
                    case "1":
                        ReadFromeFile(out a, out y, out n);
                        flag = false;
                        break;
                    case "2":
                        ReadFromConsole(out n, out a, out y);
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Нет такого значения!");
                        flag = true;
                        break;

                }
            } while (flag);
            if (a != null && y != null && n > 0)
            {
                sysout(a, y, n);
                double[] x = gauss(a, y, n);
                if (x != null)
                {
                    for (int i = 0; i < n; i++)
                        Console.WriteLine($@"x[{i}]= {x[i]}");
                }
            }
            Console.ReadKey();
        }

        private static void ReadFromeFile(out double[,] a, out double[] y, out int n)
        {
            n = 0;
            a = null;
            y = null;
            Console.WriteLine("Введите имя файла");
            string filename = Console.ReadLine();
            if(File.Exists(filename))
            {
                var file = File.ReadAllLines(filename);
                try
                {
                    n = file.Length;
                    a = new double[n, n];
                    y = new double[n];
                    for (int i = 0; i < n; i++)
                    {
                        var split = file[i].Split();
                        for (int j = 0; j < split.Length; j++)
                        {
                            if (j < split.Length - 1)
                            {
                                a[i, j] = Convert.ToDouble(split[j]);
                            }
                            else
                            {
                                y[i] = Convert.ToDouble(split[j]);
                            }

                        }
                    }
                }
                catch 
                {
                    Console.WriteLine("Неправильно заполнен файл!");
                    n = 0;
                    a = null;
                    y = null;
                }
            }
            else
            {
                Console.WriteLine("Такого файла не существует!");
            }
        }

        private static void ReadFromConsole(out int n, out double[,] a, out double[] y)
        {
            Console.WriteLine("Введите количество уравнений: ");
            while (!int.TryParse(Console.ReadLine(), out n)) ;
            a = new double[n, n];
            y = new double[n];
            bool flag = false;

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    do
                    {
                        try
                        {
                            Console.WriteLine($@"a[{i}][{j}]= ");
                            a[i, j] = Convert.ToDouble(Console.ReadLine());
                            flag = false;
                        }
                        catch
                        {
                            Console.WriteLine("Не правильно введено значение! Попробуйте ещё раз");
                            flag = true;
                        }
                    } while (flag);
                }
            }
            for (int i = 0; i < n; i++)
            {
                do
                {
                    try
                    {
                        Console.WriteLine($@"y[{i}]= ");
                        y[i] = Convert.ToDouble(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Не правильно введено значение! Попробуйте ещё раз");
                        flag = true;
                    }
                } while (flag);
            }
        }
    }
}
